package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int maxLength = 0;

        maxLength = Math.max(word1.length(), maxLength);
        maxLength = Math.max(word2.length(), maxLength);
        maxLength = Math.max(word3.length(), maxLength);


        if (word1.length() == maxLength) {
            System.out.println(word1);
        }
        if (word2.length() == maxLength) {
            System.out.println(word2);
        }
        if (word3.length() == maxLength) {
            System.out.println(word3);
    }
        

    }

    public static boolean isLeapYear(int year) {

        if (year % 4 == 0) {
            if (year % 100 == 0 && 400 != 0 && year != 2000) {
                return false;
            }
            return true;
        }
        return false;


    }

    public static boolean isEvenPositiveInt(int num) {

        if (num > 0) {
            if (num % 2 == 0) {
                return true;
            }
            return false;
        }
        return false;

    }

}
