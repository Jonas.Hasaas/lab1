package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);

    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {

        if (grid.isEmpty() || grid.get(0).isEmpty()) {
            return false;
        }

        int rowSum = 0;

        // Check rows and calculate rowSum for the first row
        for (int i = 0; i < grid.size(); i++) {
            int currentRowSum = 0;

            for (int num : grid.get(i)) {
                currentRowSum += num;
            }

            if (i == 0) {
                rowSum = currentRowSum;
            } else if (currentRowSum != rowSum) {
                return false;
            }
        }

        // Check columns
        for (int j = 0; j < grid.get(0).size(); j++) {
            int colSum = 0;

            for (ArrayList<Integer> row : grid) {
                colSum += row.get(j);
            }

            if (colSum != rowSum) {
                return false;
            }
        }

        return true;

    }

}