package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {

        ArrayList<Integer> myList = new ArrayList<>();

        for (int element : list) {
            myList.add(element * 2);
        }
        return myList;
        
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        list.removeIf(num -> num.equals(3)); {
            return list;
        }
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {

        ArrayList<Integer> uniqueList = new ArrayList<>();

        for (Integer element : list) {
            if (!uniqueList.contains(element)) {
                uniqueList.add(element);
            }
        }

        return uniqueList;

    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        
        if (a.size() != b.size()) {
            throw new IllegalArgumentException("Listene må ha samme lengde");
        }

        for (int i = 0; i < a.size(); i++) {
            a.set(i, a.get(i) + b.get(i));
        }
    }

}